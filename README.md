# ![Piratpartiet][pp_logo]
Här samlar vi de saker som behöver åtgärdas med hemsidan för partiet.

Sidan är tillgänglig live på:

- master: <https://piratpartiet.se>
- develop: Ej tillgänglig, körs endast lokalt av utvecklare. Vill du se mer om hur man sätter upp en lokal miljö så kan du följa [vår guide][pp_kod].

## Tema
Vi använder ett tema som heter [pp_www][pp_www], som ar baserat på [_s][_s]. Temat är utrensat på allt som inte ingår, eller ska ingå i vårt tema, vilket gör att vi får ett eget tema där vi själva kan sätta upp regler och arbeta fram abetssätt på hur saker skall skrivas, formateras, osv.

Temafilerna ligger här på GitLab som resurs och så man kan följa arbetet. Det underlättar även sammarbetet och att fler kan hjälpa till.

Detta tema använder sig av [ACF Pro][acf-pro] och kommer inte fungera korrekt utan det. Våra egna fält finns tillgängliga i temat under `acf-json` katalogen.

## Kodstandard
För att underlätta arbetet så ska man följa vissa arbetssätt ifall man jobbar på detta projekt. Bland annat följer vi [PSR-12][psr12]. Mer om det i dokumentet: [CONTRIBUTING.md][pp_kod]

## Att göra
Vi använder [issues][issues] här på GitLab för att hålla reda på vad som behöver åtgärdas och vad som ska göras. Vi utvecklare brukar också chatta på Piratpartiets interna chatt: [IT-Utveckling](https://chat.piratpartiet.se/pp/channels/it-utveckling). Endast medlemmar i Piratpartiet eller Ung Pirat har tillgång till chatten, för att få åtkomst till IT-Utvecklings kanalen så du fråga antingen Mattias Rubenson eller Jakob Sinclair.

## Lokala sidor
* Göteborg - Stefan Roudén har kontaktats om vem som ska få inlogg
* Lund - Jan-Erik Malmquist har kontaktats om vem som ska få inlogg
* Skåne - Jan-Erik Malmquist har kontaktats om vem som ska få inlogg
* Malmö - Christoffer Eldengrip har kontaktats om vem som ska få inlogg
* Örebro - Andreas Palmqvist har fått inlogg
* Stockholms Stad - Anders Erkéus, Mats Philipson och Gagarin Miljkovich har fått inlogg
* Stockholmsregionen - Anders Erkéus, Mats Philipson och Gagarin Miljkovich har fått inlogg

## Licens
![GNU Affero General Public License v3][affero]
```
pp_www - A WordPress theme for the Swedish Pirate Party
Copyright (C) 2020 Piratpartiet

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
See [`LICENSE`](https://gitlab.com/piratpartiet-se/piratpartiet.se/blob/develop/LICENSE) for more information.

**Notice**: This is only the license for the code of the WordPress theme.
Content posted on [piratpartiet.se](https://piratpartiet.se) doesn't follow this license.

<!-- Länkar och bilder -->
[acf-pro]: https://www.advancedcustomfields.com/pro/
[pp_logo]: pp_www/img/pp_logga_liggande_lila_svart.svg
[pp_www]: #
[_s]: https://underscores.me/
[pp_kod]: ./CONTRIBUTING.md
[psr12]: https://www.php-fig.org/psr/psr-12/
[issues]: https://gitlab.com/piratpartiet-se/piratpartiet.se/issues
[affero]: https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/AGPLv3_Logo.svg/320px-AGPLv3_Logo.svg.png