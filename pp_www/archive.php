<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pp_www
 */
get_header();

$featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
$default_img_url = get_template_directory_uri() . '/img/pp_header_default_clean_3000x1000px.png';
$featured_img_url = $featured_img_url ?: $default_img_url;

$archive_title = get_the_archive_title();

if (strstr($archive_title, 'Pressmeddelanden')) {
    $archive_title = str_replace('Kategori: ', '', $archive_title);
}

if (strstr($archive_title, 'Etikett')) {
    $archive_title = str_replace('Etikett:', 'Nyheter om:', $archive_title);
}

?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main single-news">
            <div class="feature archive-header" style="background-image: url(<?= $featured_img_url; ?>);"
                 alt="<?= $post->post_title; ?>"></div>
            <div class="full-width-banner">
                <h3 class="_text"><?= $archive_title; ?></h3>
            </div>
            <section class="news-list adjust">
<?php

$archive_description = get_the_archive_description() ?? '';

if (!empty($archive_description)) {
    echo <<< TEXTBLOCK
            <div class="text-block archive-description adjust">
                <div class="col-xs-12 offset-xs-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                    {$archive_description}
                </div>
            </div>

TEXTBLOCK;

}

if (have_posts()) {
    /* Start the Loop */
    while (have_posts()) {
        the_post();

        // Inkludera vår egen “template-parts” för kategorisidorna
        get_template_part('template-parts/content', 'category');
    }

    the_posts_navigation();

// ev ta bort denna sen...
} else {
    get_template_part('template-parts/content', 'none');
}
?>
            <section><!-- .news-list -->
        </main><!-- #main -->
    </div><!-- #primary -->
<?php
get_footer();
