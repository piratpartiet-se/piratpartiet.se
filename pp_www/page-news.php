<?php
/**
 * Template Name: Enkel sida + nyheter
 *
 * Template Description: Det här är en variant av mallen för enkla sidor med möjlighet att lägga
 * in nyhetsblocket vi modulen. Obs! inga andra moduler fungerar. Använd
 * Gutenberg (blocksystemet) för att lägga in videos osv.
 *
 * /E
 *
 * @package pp_www
 */
get_header();

$featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
$default_img_url = get_template_directory_uri() . '/img/pp_header_default_clean_3000x1000px.png';
$featured_img_url = $featured_img_url ?: $default_img_url;

$post_title = $post->post_title;
$post_content = apply_filters('the_content', $post->post_content);

$header_text = "";
$has_newslist = false;

?>
<?php

// check if the flexible content field has rows of data
if (have_rows('modules')) {
    // loop through the rows of data
    while (have_rows('modules')) {
        the_row();
        if (get_row_layout() == 'fullbreddsbild') {
            $header_text = get_sub_field('title') ?: get_the_title();
            $img = get_sub_field('img');
            if ($img) {
                $featured_img_url = $img['sizes']['large'] ?: $featured_img_url;
            }
        } else if (get_row_layout() == 'news_list') {
            $has_newslist = true;
            $news_title = get_sub_field('news_list_title');
            $post_count = get_sub_field('post_count');
            $post_order = get_sub_field('order_asc_desc');
            $anchor = get_sub_field('anchor');
            $filter_by_tag = get_sub_field('filter_by_tag') ?: '';

            $no_posts_msg = get_sub_field('no_posts_msg')
                                ?: 'För närvarande finns det inga nyheter om %1s.';     // ifall-ifall
            $no_posts_msg = sprintf($no_posts_msg, $filter_by_tag);
        }
    }
}
?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main single-news">
            <div class="feature" style="background-image: url(<?= $featured_img_url ?>)" title="<?= $post_title ?>">
            <?php
            if ($header_text) {
            ?>
                <section>
                    <article class="header-text">
                        <h1 class="header_text_h1"><?= $header_text ?></h1>
                    </article>
                </section>
            <?php
            }
            ?>
            </div>
            <div class="full-width-banner">
                <h3 class="_text"><?= $post_title ?></h3>
            </div>
            <div class="_content col-xs-12 offset-xs-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                <?= $post_content ?>
            </div>
            <?php
            if ($has_newslist == true) {
                require_once 'acf-modules/newslist.php';
                render_newslist($post_count, $post_order, $no_posts_msg, $filter_by_tag, TRUE, $news_title, $anchor);
            }
            ?>
        </main>
    </div>
<?php
get_footer();