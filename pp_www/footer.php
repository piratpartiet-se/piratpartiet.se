<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pp_www
 */

// Network nav
$menu_name = 'network';

$network_menu = wp_nav_menu([
    'container'         => 'nav',
    'container_class'   => $menu_name . ' site-info clearfix',  // nav.network
    'container_id'      => $menu_name,                          // nav#network
    'menu_class'        => $menu_name . '-menu',                // nav ul.network-menu
    'menu_id'           => $menu_name . '-menu',                // nav ul#network-menu
    'echo'              => false,                               // use return
    'theme_location'    => $menu_name,
]);

// Följ oss…
$menu_follow_us = 'follow-us';
$menu_list = '';

if ($locations = get_nav_menu_locations()) {
    $menu = wp_get_nav_menu_object($locations[$menu_follow_us]);
    $menu_items = wp_get_nav_menu_items($menu->term_id);
    $menu_follow_us_title = $menu->name;

    $menu_list = <<< MENUSTART

        <h3>{$menu_follow_us_title}</h3>
        <ul id="{$menu_follow_us}-menu">

MENUSTART;

    foreach ((array) $menu_items as $key => $menu_item) {
        $title = $menu_item->title;
        $fa_class = $menu_item->classes[0];
        $fa_category = $fa_class == "rss" ? "fas" : "fab";
        $url = $menu_item->url;
        $title = sprintf('Följ oss på %1s', $title);
        $menu_list .= <<< MENUITEMS
            <li><a href="{$url}" alt="{$title}" title="{$title}" class="{$fa_category} fa-{$fa_class}"></a></li>

MENUITEMS;
    }
    $menu_list .= str_repeat("\t", 2) .'</ul>';
}

$follow_us_menu = $menu_list ?: '';

/**
 * Textinnehållet i footern kommer från tillägget “Footer Text”:
 * https://wordpress.org/plugins/footer-text/
 *
 * Skapas i WP-admin under: Utseende->Footer Text.
 * Tänk på att skriva/kontrollera html
 */
$footer_text_content = get_footer_text();

// Footer info

$_site_url = get_site_url();
$_site_name = get_bloginfo();
$_tpl_dir = get_template_directory_uri();

$footer_site_url = sprintf('<a href="%1s" title="%2s"></a>', $_site_url, $_site_name);
$footer_policy_url = sprintf('<a href="%1s" title="Integritetspolicy">Integritetspolicy</a>.', '/integritetspolicy/');
$cc_by_url = sprintf('<a href="%1s"></a>', 'https://creativecommons.org/licenses/by/4.0/legalcode.sv');
$footer_info = sprintf('<a href="%1s" title="%2s">%3s</a>, %4s.', $_site_url, $_site_name, $_site_name, date('Y'));

$footer_logo_bgnd = $_tpl_dir . '/img/pp_logga_liggande_lila_vit.svg';
$cc_by_bgnd = $_tpl_dir . '/img/cc_by.svg';

echo <<< FOOTER
    </div><!-- #content -->
    {$network_menu}<!-- .network -->
</div><!-- #page -->
<footer id="footer" class="site-footer">
    <div class="follow-us">
        {$follow_us_menu}
    </div><!-- .follow us -->
    <div class="footer-logo" style="background-image: url({$footer_logo_bgnd});">
        {$footer_site_url}
    </div>
    <div class="footer-content">
        {$footer_text_content}
    </div>
    <div class="cc-by" title="Creative Commons CC-BY 4.0" style="background-image: url({$cc_by_bgnd});">
        {$cc_by_url}
    </div>
    <div class="footer-info">
        <p>{$footer_info} {$footer_policy_url}</p>
    </div><!-- .site-info -->
</footer><!-- #footer -->

FOOTER;

// wp_footer() *måste* vara inom sina egna php-taggar
?>
<?php wp_footer(); ?>
    <script>
        jQuery('p:empty').remove();
        $(function() {
            const desktopSize = 1285;
            var body = $(document.body);
            var page = $('#page');
            var logo_holder = $('header .logo_holder');
            var scrollTop = 0;
            var scrollVerticalLength = 400;
            var screenWidth = 0;
            var screenHeight = 0;
            var logoWidth = 0;
            var logoFactor = 0;
            var scrollbarWidth = getScrollbarWidth();
            var sliderExists = document.getElementById('slider');
            var stopControllingMenu = false;

            if (sliderExists == null) {
                sliderExists = false;
            } else {
                sliderExists = true;
            }

            updatePageVars();

            $(window).scroll(function() {
                setLogoStyle();
                if (screenWidth >= desktopSize && sliderExists == true && stopControllingMenu == false) {
                    if (scrollTop > 500) {
                        openMenu();
                    } else {
                        closeMenu();
                    }
                }
            });

            $(window).resize(function() {
                updatePageVars();
                setLogoStyle();
            });

            setLogoStyle();

            var hamburger = $('#hamburger');
            var nav = $('#site-navigation');

            hamburger.on('click', function(event) {
                stopControllingMenu = true;
                toggleMenu();
            });

            if (screenWidth >= desktopSize && sliderExists == false) {
                openMenu();
            }

            $('#primary-menu li.menu-item a').on('click', function(event) {
                var hrefArr = event.target.href.split('/');
                var href = hrefArr[hrefArr.length-1];
                if (href.indexOf('#') !== -1) {
                    $('html, body').animate({
                       'scrollTop': $(href).offset().top
                     }, 1000, 'easeInOutQuint', function() {

                     });
                     toggleMenu();
                }
            });

            function toggleMenu() {
                hamburger.toggleClass('open');
                nav.toggleClass('open');
                body.toggleClass('menu-open');

                if (body.hasClass('menu-open') && screenWidth < desktopSize) {
                    page.css('margin-right', scrollbarWidth + 'px');
                } else {
                    page.css('margin-right', '0');
                }
            }

            function openMenu() {
                hamburger.addClass('open');
                nav.addClass('open');
                body.addClass('menu-open');

                if (screenWidth < desktopSize) {
                    page.css('margin-right', scrollbarWidth + 'px');
                } else {
                    page.css('margin-right', '0');
                }
            }

            function closeMenu() {
                hamburger.removeClass('open');
                nav.removeClass('open');
                body.removeClass('menu-open');

                page.css('margin-right', '0');
            }

            function updatePageVars() {
                screenWidth = page.width();
                screenHeight = $(window).height();

                if (screenHeight > 600) {
                    if (screenWidth < 700) {
                        logoWidth = screenWidth / 4;
                    }
                    if (screenWidth > 700) {
                        logoWidth = screenWidth / 6;
                    }
                    if (screenWidth > 1000) {
                        logoWidth = screenWidth / 8;
                    }
                } else {
                    logoWidth = screenWidth / 8;
                }
                logoFactor = screenWidth < logoWidth ? screenWidth : logoWidth;
            }

            function setLogoStyle() {
                scrollTop = $(window).scrollTop();
                if (scrollTop < scrollVerticalLength - 100) {
                    logoWidth = logoFactor * ((124 - ((scrollTop/scrollVerticalLength) * 100)) / 100);
                    logo_holder.css({ 'width': (logoWidth) + 'px' }).removeClass('small');
                } else {
                    logo_holder.addClass('small');
                }
            }

            function getScrollbarWidth() {
                var outer = document.createElement('div');
                outer.style.visibility = 'hidden';
                outer.style.width = '100px';
                outer.style.msOverflowStyle = 'scrollbar'; // needed for WinJS apps

                document.body.appendChild(outer);

                var widthNoScroll = outer.offsetWidth;
                // force scrollbars
                outer.style.overflow = 'scroll';

                // add innerdiv
                var inner = document.createElement('div');
                inner.style.width = '100%';
                outer.appendChild(inner);

                var widthWithScroll = inner.offsetWidth;

                // remove divs
                outer.parentNode.removeChild(outer);

                return widthNoScroll - widthWithScroll;
            }
        });
    </script>
</body>
</html>
