/*
* FlowType.JS v1.1
* Copyright 2013-2014, Simple Focus http://simplefocus.com/
*
* FlowType.JS by Simple Focus (http://simplefocus.com/)
* is licensed under the MIT License. Read a copy of the
* license in the LICENSE.txt file or at
* http://choosealicense.com/licenses/mit
*
* Thanks to Giovanni Difeterici (http://www.gdifeterici.com/)
*/
(function($) {
    $.fn.flowtype = function(options) {

        // Establish default settings/variables
        var settings = $.extend({
                maximum   : 9999,
                minimum   : 1,
                maxFont   : 9999,
                minFont   : 1,
                fontRatio : 35
            }, options),

            // Do the magic math
            changes = function(el) {
                var $el = $(el),
                    elw = $el.width(),
                    width = elw > settings.maximum ? settings.maximum : elw < settings.minimum ? settings.minimum : elw,
                    fontBase = width / settings.fontRatio,
                    fontSize = fontBase > settings.maxFont ? settings.maxFont : fontBase < settings.minFont ? settings.minFont : fontBase;
                $el.css('font-size', fontSize + 'px');
            };

        // Make the magic visible
        return this.each(function() {
            // Context for resize callback
            var that = this;
            // Make changes upon resize
            $(window).resize(function(){changes(that);});
            // Set changes on load
            changes(this);
        });
    };
}(jQuery));

jQuery(function() {
    var _maxFont = 80;
    _maxFont = ($(window).height() < 500) ? 35 : _maxFont;
    _maxFont = ($(window).height() > 500 && $(window).height() < 700) ? 50 : _maxFont;

    jQuery('.big_header h1').flowtype({
        minimum: 300,
        maximum: 2000,
        minFont: 24,
        maxFont: _maxFont,
        fontRatio: 8
    });

    jQuery('.header-text h1').flowtype({
        minimum: 300,
        maximum: 2000,
        minFont: 24,
        maxFont: _maxFont,
        fontRatio: 8
    });

    jQuery('h3 ._text').flowtype({
        minimum: 300,
        maximum: 2000,
        minFont: 27,
        maxFont: 40,
        fontRatio: 30
    });

    jQuery('.button.local-department-button p').flowtype({
        minimum: 300,
        maximum: 2000,
        minFont: 12,
        maxFont: 70,
        fontRatio: 18
    });
});