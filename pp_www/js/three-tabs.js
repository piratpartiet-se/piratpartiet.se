jQuery(function() {
    var tabContentPanels = jQuery('.tab_content_panel');

    jQuery('#content').on('click touchstart', '.tab_buttons .tab_button', function(event) {
        event.preventDefault();
        var target = $(event.target).closest('.tab_button');
        var tabID = target.data('tabid');
        var tabContentPanel = jQuery(this.parentElement.parentElement.parentElement.getElementsByClassName("tab_content_panel"));
        setActiveButton(target, tabContentPanels);
        showTab(tabID, tabContentPanel);
    });

    var big_header = jQuery('.big_header .buttons_banner_container');
    var news_slides = jQuery('.news-slide');
    var rubrik_header = jQuery('.rubrik_header');
    var tab_buttons = jQuery('.tab_buttons');

    setModuleDimensions();

    jQuery(window).resize(function() {
        setModuleDimensions();
    });

    function setModuleDimensions() {
        var screen_height = window.innerHeight;
        var tabs_height = tab_buttons.outerHeight();
        var rubrik_height = rubrik_header.outerHeight();
        big_header.css({
            'height': screen_height - (tabs_height + rubrik_height) + 'px'
        });
        news_slides.css({
            'height': screen_height - (tabs_height + rubrik_height) + 'px'
        });
    }

    function showTab(tabID, tabContent) {
        currentTab = tabContent.find('._content.' + tabID);
        if (currentTab.hasClass('shown')) {
            currentTab.removeClass('shown');
        } else {
            tabContent.find('._content').removeClass('shown');
            currentTab.addClass('shown');
        }
        setPanelHeight(tabContent);
    }

    function setActiveButton(target, panels) {
        var currentButton = target.parents('.three-tabs').find('.tab_button');
        if (target.hasClass('active')) {
            target.removeClass('active');
        } else {
            currentButton.removeClass('active');
            target.addClass('active');
        }

        if ($(window).scrollTop() + $(window).height() < ($(target).offset().top + $(target).height()) + 150) {
            $("html, body").animate({
                scrollTop: ($(target).offset().top - 65)
            }, 250);
        }
    }

    function setPanelHeight(tabContent) {
        var shownPanel = tabContent.parent().find('._content.shown');
        if (shownPanel.length !== 0) {
            tabContent.css('height', shownPanel.outerHeight() + 'px');
        } else {
            tabContent.css('height', '0px');
        }
    }
});