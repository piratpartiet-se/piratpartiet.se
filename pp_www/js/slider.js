jQuery(function() {
    $('.slideshow').carousel();
    var fullImageSlidesCount = document.getElementsByClassName('full-image-slides').length;

    $('.slideshow').on('slide.bs.carousel', function (event) {
        if (event.to >= fullImageSlidesCount) {
            $('#slider .carousel-indicators').addClass('news');
        } else {
            $('#slider .carousel-indicators').removeClass('news');
        }
    });
});