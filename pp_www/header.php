<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pp_www
 */
//language_attributes()
$lang_attrib = get_language_attributes('xhtml'); // . ' xmlns="http://www.w3.org/1999/xhtml"';

$pp_segel_big = get_template_directory_uri() . '/img/pp_segel_vit_vit.svg';
$pp_segel_small = get_template_directory_uri() . '/img/vitt_segel.svg';

$favicon = get_template_directory_uri() . '/img/favicon.png';
$apple_touch = get_template_directory_uri() . '/img/apple-touch-icon.png';


?>
<!doctype html>
<html <?= $lang_attrib; ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="referrer" content="no-referrer">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
<?php
    get_template_part('inc/header', 'xtras');
?>

<link rel="icon" type="image/png" href="<?= $favicon; ?>">
<link rel="apple-touch-icon" sizes="180x180" href="<?= $apple_touch; ?>">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Hoppa till innehåll', 'pp_www'); ?></a>
    <header id="masthead" class="site-header">
        <div class="site-branding">
            <div class="logo_holder small">
                <a href="/" title="Piratpartiet Startsida">
                    <div class="logo">
                        <img class="big" src="<?= $pp_segel_big; ?>" />
                        <img class="small" src="<?= $pp_segel_small; ?>" />
                    </div>
                </a>
            </div>
        </div><!-- .site-branding -->
        <nav id="site-navigation" class="main-navigation">
            <?php
            wp_nav_menu([
                'theme_location' => 'main',
                'menu_id'        => 'primary-menu',
            ]);
            ?>
        </nav><!-- #site-navigation -->
    </header><!-- #masthead -->
    <div id="hamburger_holder">
        <div id="hamburger">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div id="content" class="site-content">
