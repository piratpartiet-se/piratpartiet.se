<?php
/**
 * Header Xtras
 *
 * Det här är en separat fil för olika meta-properties
 * som “og” och “twitter”. Men även för att kunna lägga till annan
 * info som man kanske vill ha i egen fil.
 *
 * Filen inkluderas i header.php:
 *
 *     get_template_part('inc/header', 'xtras');
 *
 * @package pp_www
 */

/**
 * https://mekshq.com/get-current-page-url-wordpress/
 * $meta_slug = add_query_arg([], $wp->request);
 */

$search_url = '';
$meta_image = '';

// Object ID & term
$obj_id = get_queried_object_id();
$get_term = get_term_by('id', $obj_id, 'category');

// Piratpartiet i text
$pp_name = get_bloginfo('name');
$pp_title = 'Omtänksamhet istället för misstänksamhet';

if ($search_query = get_search_query()) {
    $search_url = get_search_link($query);
}

// Canonical URL
$rel_canonical = $search_url ?: home_url(add_query_arg([], $wp->request) . '/')
                 ?: get_permalink() ?: get_site_url();

/**
 * Förvalda värden som vi både
 * använder som default &/el fallback
 */
$default_meta_array = $dma = [
    'locale'        => get_locale(),
    'twitter'       => '@' . strtolower($pp_name),
    'tw_card'       => 'summary',
    'url'           => get_site_url(),
    'type'          => 'website',
    'image'         => get_template_directory_uri() . '/img/piratpartiet.png',
    'image_alt'     => $pp_title,
    'title'         => $pp_title,
    'description'   => 'Piratpartiet grundades 2006 för att föra Sverige in i den digitala framtiden. Vi lyfter frågor som inga andra partier pratar om, som behovet av privatliv, stärkt svensk digital infrastruktur och vad som händer med arbetsmarknaden i och med automatisering. Vi finns idag i ett sextiotal länder och har utvecklats till världens mest framgångsrika politiska rörelse sedan miljörörelsen på 80-talet.',
];

if ($search_query) { // Söksidan
    $meta_title = esc_html__('Sökresultat', 'pp_www');
    $meta_description = sprintf(esc_html__('Visar sökresultat för: %s', 'pp_www'), '<span class="search">' . $search_query . '</span>');
} elseif (is_string(get_term_link($obj_id))) { // Titel på kategorisidorna
    $meta_title = $get_term->name ?: $meta_title ?: '';
    $meta_description =  $get_term->description;

    $meta_image = get_template_directory_uri() . '/img/piratpartiet_1080x540px.png';
} else {
    $meta_title = get_the_title();
    $meta_description = $post->post_excerpt;

    if (has_post_thumbnail()) {
        $meta_image = get_the_post_thumbnail_url($post->ID, 'full');
    }
}

// Värden för andra sidor/inlägg än “Home”
$meta_array = [
    'locale'        => '',
    'twitter'       => '',
    'tw_card'       => '',
    'url'           => $rel_canonical ?: '',
    'type'          => '',
    'image'         => $meta_image ?: '',
    'image_alt'     => wp_strip_all_tags($meta_title) ?: '',
    'title'         => wp_strip_all_tags($meta_title) ?: '',
    'description'   => wp_strip_all_tags($meta_description) ?: '',
];

// Använd fallback om det inte finns någon data
foreach ($meta_array as $meta => $property) {
    if (is_front_page() || is_home()) {
        $meta_array[$meta] = $default_meta_array[$meta];
    } else {
        $meta_array[$meta] = (!empty($property)) ? $meta_array[$meta] : $default_meta_array[$meta];
    }
}

extract($meta_array, EXTR_PREFIX_ALL, 'mprop');
// https://neilpatel.com/blog/open-graph-meta-tags/ (bra info)
?>
    <link rel="canonical" href="<?= $mprop_url; ?>" />
    <meta property="og:locale"      content="<?= $mprop_locale; ?>" />
    <meta property="og:title"       content="<?= $mprop_title; ?>" />
    <meta property="og:description" content="<?= $mprop_description; ?>" />
    <meta property="og:url"         content="<?= $mprop_url; ?>" />
    <meta property="og:type"        content="<?= $mprop_type; ?>" />
    <meta property="og:image"       content="<?= $mprop_image; ?>" />
    <meta property="og:image:alt"   content="<?= $mprop_image_alt; ?>" />
    <meta property="og:site_name"   content="<?= $pp_name; ?>">
    <meta name="twitter:card"       content="<?= $mprop_tw_card; ?>" />
    <meta property="twitter:site"   content="<?= $mprop_twitter; ?>" />
