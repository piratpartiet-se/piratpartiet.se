=== pp_www ===

Contributors: Piratpartiet
Tags: custom-background, custom-logo, custom-menu, featured-images, threaded-comments, translation-ready

Requires at least: 4.5
Tested up to: 5.6.0
Stable tag: 1.6.0
License: GNU Affero General Public License v3 (AGPLv3)
License URI: LICENSE

Ett tema som heter pp_www avsett för piratpartiet.se.

== Description ==

Det här temat heter <code>pp_www</code> och är baserat på temat <code>_s</code>. Det är ett tema avsett för att \"hackas\", så använd mig inte som <em>Parent Theme</em>. Temat utvecklas lokalt och är <strong>endast</strong> avsett för användning av/för piratpartiet.se

Utvecklingen sker på GitLab, där rapporterar vi buggar och förslag på ändringar/förbättringar.

GitLab repo: <a href="https://gitlab.com/piratpartiet-se/piratpartiet.se">piratpartiet.se</a>


== Changelog ==

= 1.6.0 - 12 mars, 2021 =
* Lägg till donationslänkar och "Bli pirat!" knapp till nyhetsinlägg

= 1.5.1 - 22 december, 2020 =
* Lägg till publiceringsdatum på nyheter

= 1.5.0 - 7 december, 2020 =
* Lägg till stöd för Discord och RSS ikon bland sociala medier
* Snygga till "Senaste nytt"-sidan
* Förbättra CSS utan JavaScript
* Gör sidomenyn större och förbättra beteende
* Byt ut Invidious-länkar mot YouTube-länkar igen

= 1.4.3 - 24 mars, 2020 =
* Fixade radhöjd i menyn

= 1.4.2 - 24 mars, 2020 =
* Fixade fel som påverkade enkla sidor och nyheter

= 1.4.1 - 24 mars, 2020 =
* Fixade fel som orsakade att man inte kunde ha multipla moduler av vissa typer

= 1.4.0 - 24 mars, 2020 =
* Flera buggfixar för slideshowen
* Gör menyn mindre och öppna den automatiskt för stora skärmar
* Gör det möjligt att lägga till rubriker till enkla sidor, med hjälp av 'fullbreddsbild' modulen
* Se till att JavaScript-kod laddas efter all html
* Oanvänd PHP och JavaŚcript-kod borttagen

= 1.3.1 - 3 mars, 2020 =
* Flera buggfixar för slideshowen
* Utdrag ifrån nyheter visar max 500 tecken nu i slideshowen
* Indikatorerna flyttar nu upp och ned ifall man går mellan en stor bild och en nyhet i slideshowen

= 1.3.0 - 1 mars, 2020 =
* Lade till en komponent, en slideshow, som är tänkt att användas på framsidan av hemsidan
* Uppdaterade bootstrap-grid.css till senaste versionen, v4.4.1
* Lade till bootstrap.js, används för slideshow komponenten
* Rensade bort JavaScript bibliotek och uppdaterade biblioteken som blev kvar

= 1.2.2 - 11 februari, 2020 =
* Fixade bugg som gjorde att man inte kunde sätta utvald bild för en lokal video

= 1.2.1 - 10 februari, 2020 =
* Fixade bugg som gjorde att bara en video kunde spelas upp per sida

= 1.2.0 - 10 februari, 2020 =
* Städade bort oanvända funktioner och fält, till exempel dragend.js och kalender modulen
* Bytte tillbaka Invidious-länkar till deras vanliga format
* Lade till nytt block för videos som hanterar videoklipp som är direkt uppladdade på WordPress-instansen

= 1.1.1 - 8 februari, 2020 =
* Bytte Invidious-länkar till RAW-format

= 1.1 - 16 januari, 2020 =
* YouTube-länkar utbytta till Invidious-länkar
* Lägg till ACF konfigurationen direkt i temat
* Övriga små fixar

= 1.0 - 24 februari, 2019 =
* Temat _s omdöpt till pp_www och uppladdat på GitLab

== Credits ==

* Baserat på _s och Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 eller senare](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)
