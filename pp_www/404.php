<?php
/**
 * Mallen för att visa 404-sidor (Not found)
 *
 * Sidan är omgjord för att anpassas till temat, samt tagit bort
 * all blandad kod (php+html i samma fil), och kör istället med
 * 'HEREDOC'. (Same-same… men renare)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package pp_www
 */
get_header();

$featured_img_url = NULL; //get_the_post_thumbnail_url(get_the_ID(), 'full');
$default_img_url = get_template_directory_uri() . '/img/pp_header_default_clean_3000x1000px.png';
$featured_img_url = $featured_img_url ?: $default_img_url;

$page_titel = esc_html__('Oops! Den sidan kunde inte hittas.', 'pp_www');
$page_intro = esc_html__('Filen eller sidan saknas. Du kan kanske kan prova med någon av länkarna nedanför, eller sök på sidan?', 'pp_www');
$widget_cat_title = esc_html__('Mest använda kategorier', 'pp_www');
$pp_www_archive_content = esc_html__('Titta i månadsarkiven.', 'pp_www');

$search_form = get_search_form(false);

$widget_cat_list = wp_list_categories([
    'echo'          => 0,
    'orderby'       => 'count',
    'order'         => 'DESC',
    'show_count'    => 1,
    'title_li'      => '',
    'number'        => 10,
]);

echo <<< OUTPUT
    <div id="primary" class="content-area">
        <!--<main id="main" class="site-main">-->
        <main id="main" class="site-main single">
                        <section class="error-404 not-found">
                <div class="feature archive-header" style="background-image: url({$featured_img_url});"
                     alt="{$page_titel}" title="{$page_titel}"></div>
                <div class="full-width-banner">
                    <h3 class="_text">{$page_titel}</h3>
                </div>
                <div class="page-content col-xs-12 offset-xs-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3"">
                    <p>{$page_intro}</p>

                    {$search_form}
                    <hr class="separator" />

OUTPUT;

the_widget('WP_Widget_Recent_Posts');

echo <<< OUTPUT
                    <div class="widget widget_categories">
                        <h2 class="widget-title">{$widget_cat_title}</h2>
                        <ul>
                            {$widget_cat_list}
                        </ul>
                    </div><!-- .widget -->

OUTPUT;

the_widget('WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$pp_www_archive_content");

the_widget('WP_Widget_Tag_Cloud');

echo <<< OUTPUT
                </div><!-- .page-content -->
            </section><!-- .error-404 -->
        </main><!-- #main -->
    </div><!-- #primary -->

OUTPUT;

get_footer();