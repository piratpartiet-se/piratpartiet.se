<?php
/**
 * Template Name: Archives
 *
 * Mallfil för att visa arkivindex
 *
 * @link https://codex.wordpress.org/Creating_an_Archive_Index
 *
 * @package pp_www
 */
get_header();

$featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
$default_img_url = get_template_directory_uri() . '/img/pp_header_default_clean_3000x1000px.png';
$featured_img_url = $featured_img_url ?: $default_img_url;

$page_title = get_the_title();

$archive_description = apply_filters('the_content', $post->post_content)
                       ?: '<p>Beskrivning/intro</p>';

$search_description = 'Om du inte hittade något ovanför, så kan du söka på sidan istället.';

$archives_array = [
    'yearly'        => wp_get_archives('type=yearly&show_post_count=true&echo=0'),
    'monthly'       => wp_get_archives('type=monthly&show_post_count=true&echo=0'),
    'categories'    => wp_list_categories('echo=0&hide_empty=0&show_count=1'),
    'tags'          => wp_tag_cloud([
       'smallest'   => 14,
       'separator'   => ',',
       'largest'    => 28,
       'unit'       => 'px',
       'number'     => 45,
       'orderby'    => 'name',
       'order'      => 'ASC',
       //'taxonomy'   => 'post_tag',
       'echo'       => false,
       'show_count' => 'post_tag',
    ]),

];

extract($archives_array, EXTR_PREFIX_ALL, 'archives');

$archives_tags = $archives_tags ?: '<em class="empty">Tagg-molnet är tomt.</em>';

$search_form = get_search_form(false);

echo <<< OUTPUT
    <div id="primary" class="content-area">
        <main id="main" class="site-main single-news">
            <div class="feature archive-header" style="background-image: url({$featured_img_url});"></div>
            <div class="full-width-banner">
                <h3 class="_text">{$page_title}</h3>
            </div>
            <section class="news-list">

OUTPUT;

if (!empty($archive_description)) {

    echo <<< INTRO
                <div class="text-block archive-description">
                    <div class="col-xs-12 offset-xs-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                        {$archive_description}
                    </div>
                </div>
INTRO;

}

if (have_posts()) {
    the_post();

    if ($post->post_name == 'arkiv') {
        echo <<< ARKIV
                <article>
                    <div class="col-xs-12 offset-xs-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                        <h2>Inlägg årsvis:</h2>
                        <ul>
                             {$archives_yearly}
                        </ul>

                        <h2>Inlägg månadvis:</h2>
                        <ul>
                             {$archives_monthly}
                        </ul>

                        <h2>Inlägg per kategori:</h2>
                        <ul>
                             {$archives_categories}
                        </ul>

                        <h2>Etiketter:</h2>
                        <p>{$archives_tags}</p>
                    </div>
                </article>
ARKIV;

        $other_archive_msg = $a_msg = 'Gå till';
        $other_archive_name = $a_name = 'Senaste nytt';
        $other_archive_url = $a_url ='/senaste-nytt/';

    } elseif ($post->post_name == 'senaste-nytt') {
        require_once get_template_directory() . '/acf-modules/newslist.php';
        render_newslist(100, 'DESC', "Inga nyheter har publicerats än", "", FALSE, "", "", FALSE);
        $other_archive_msg = $a_msg = 'För äldre nyheter, gå till';
        $other_archive_name = $a_name = 'Nyhetsarkivet';
        $other_archive_url = $a_url ='/arkiv/';
    }

    echo <<< SEARCHBOX
                <hr class="separator"/>
                <article>
                    <div class="col-xs-12 offset-xs-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                        <p>{$a_msg} <a href="{$a_url}" title="{$a_name}">{$a_name}&hellip;&raquo;</a></p>
                        <hr class="separator"/>
                        <h2>Sök på sidan</h2>
                        <p>{$search_description}</p>
                        {$search_form}
                    </div>
                </article>

SEARCHBOX;

} else {
    // Fallback till content-none.php
    get_template_part('template-parts/content', 'none');
}
?>
            </section>
        </main>
    </div>
<?php
get_footer();
