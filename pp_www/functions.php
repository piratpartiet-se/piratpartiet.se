<?php
/**
 * pp_www functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package pp_www
 */
setlocale(LC_ALL, 'sv_SE.UTF-8', 'sv_SE', 'sv');

if (!function_exists('pp_www_setup')) {
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function pp_www_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on pp_www, use a find and replace
         * to change 'pp_www' to the name of your theme in all the template files.
         */
        load_theme_textdomain('pp_www', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        /**
         * This theme uses wp_nav_menu() in 3 locations.
         *
         * 1. Primär: Huvudmenyn uppe i hörnet.
         * 2. PP-Nätverk: Alla länkar till olika PP-sidor
         * 3. PP-Följ oss: Länkar till sociala medier i footern.
         */
        register_nav_menus([
            'main'      => esc_html__('Primär', 'pp_www'),
            'network'   => esc_html__('PP-Nätverk', 'pp_www'),
            'follow-us'   => esc_html__('PP-Följ oss', 'pp_www'),
        ]);

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', [
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ]);

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('pp_www_custom_background_args', [
            'default-color' => '512483',
            'default-image' => '',
        ]));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', [
            'height'      => 250,
            'width'       => 250,
            'flex-width'  => true,
            'flex-height' => true,
        ]);
    }
}

add_action('after_setup_theme', 'pp_www_setup');


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function pp_www_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters( 'pp_www_content_width', 640 );
}

add_action('after_setup_theme', 'pp_www_content_width', 0);


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function pp_www_widgets_init()
{
    register_sidebar([
        'name'          => esc_html__('Sidebar', 'pp_www'),
        'id'            => 'sidebar-1',
        'description'   => esc_html__('Add widgets here.', 'pp_www'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ]);
}

add_action('widgets_init', 'pp_www_widgets_init');


/**
 * Enqueue scripts and styles.
 *
 * Start by deregistering the WordPress jQuery so that we can use a newer version
 */
function pp_www_scripts()
{
    wp_deregister_script('jquery');
    wp_enqueue_style('pp_www-style', get_stylesheet_uri(), [], '20190301-1615', 'all');
    wp_enqueue_script('pp_www-navigation', get_template_directory_uri() . '/js/navigation.js', [], '20151215', true);
    wp_enqueue_script('pp_www-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', [], '20151215', true);
    wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery-3.6.0.min.js', [], '3.6.0', true);
    wp_enqueue_style('bootstrap-grid', get_template_directory_uri() . '/css/bootstrap-grid.min.css', [], '4.4.1', 'all');
    wp_enqueue_script('flowtype', get_template_directory_uri() . '/js/flowtype.js', [], '1.1.0', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'pp_www_scripts');

// Implement the Custom Header feature.
require get_template_directory() . '/inc/custom-header.php';

// Custom template tags for this theme.
require get_template_directory() . '/inc/template-tags.php';

// Functions which enhance the theme by hooking into WordPress.
require get_template_directory() . '/inc/template-functions.php';

// Customizer additions.
require get_template_directory() . '/inc/customizer.php';

// Funktion för att kunna duplicera sidor/inlägg
require get_template_directory() . '/inc/duplicate-post.php';

add_post_type_support('page', 'excerpt');

/**
 * Link all post thumbnails to the post permalink.
 *
 * @param string $html          Post thumbnail HTML.
 * @param int    $post_id       Post ID.
 * @param int    $post_image_id Post image ID.
 * @return string Filtered post image HTML.
 *
 * https://developer.wordpress.org/reference/functions/the_post_thumbnail/
 * https://developer.wordpress.org/reference/functions/get_the_post_thumbnail/
 */
function wpdocs_post_image_html($html, $post_id, $post_image_id)
{
    $html = '<a href="' . get_permalink($post_id) . '" alt="' . esc_attr(get_the_title($post_id)) . '">' . $html . '</a>';
    return $html;
}
add_filter('post_thumbnail_html', 'wpdocs_post_image_html', 10, 3);

// remove the unwanted <meta generator> links
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
//https://github.com/WordPress/WordPress/blob/master/wp-includes/default-filters.php#L237


// Anv tillägg: “Empty P Tag” istället
remove_filter('get_footer_text', 'wpautop');

// Ingen Adminbar
show_admin_bar(false);
