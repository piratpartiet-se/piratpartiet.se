<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package pp_www
 */
get_header();

$featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
$default_img_url = get_template_directory_uri() . '/img/pp_header_default_clean_3000x1000px.png';
$featured_img_url = $featured_img_url ?: $default_img_url;
//$archive_title = get_the_archive_title();

/* translators: %s: search query. */
$search_query = get_search_query();
$search_title = esc_html__('Sökresultat', 'pp_www');
$search_results = sprintf(esc_html__('Visar sökresultat för: %s', 'pp_www'), '<span class="search">' . $search_query . '</span>');

$search_form = get_search_form(false);
?>
    <div id="primary" class="content-area">
        <!--<main id="main" class="site-main">-->
        <main id="main" class="site-main single-news">
            <div class="feature archive-header" style="background-image: url(<?= $featured_img_url; ?>);"
                 alt="<?= $post->post_title; ?>"></div>
            <div class="full-width-banner">
                <h3 class="_text"><?= $search_title; ?></h3>
            </div>
            <section class="news-list adjust">


    <section id="primary" class="content-area">
        <!--<main id="main" class="site-main">-->
        <main id="main" class="site-main single-news">
<?php
if (have_posts()) {
    echo <<< TEXTBLOCK
            <div class="text-block archive-description adjust">
                <div class="col-xs-12 offset-xs-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                    <p>{$search_results}</p>
                    {$search_form}
                </div>
            </div>

TEXTBLOCK;

    /* Start the Loop */
    while (have_posts()) {
        the_post();

        // Inkludera vår egen “template-parts” för sökresultat
        get_template_part('template-parts/content', 'search');
    }

the_posts_navigation();

} else {
    get_template_part('template-parts/content', 'none');
}
?>
            <section><!-- .news-list -->
        </main><!-- #main -->
    </section><!-- #primary -->
<?php
get_footer();
