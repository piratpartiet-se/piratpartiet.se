<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pp_www
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if (post_password_required()) {
    return;
}
?>
<div id="comments" class="comments-area">
    <?php
    // You can start editing here -- including this comment!
    if (have_comments()) {
        ?>
        <h2 class="comments-title">
            <?php
            $pp_www_comment_count = get_comments_number();
            if ('1' === $pp_www_comment_count) {

                // translators: 1: title.
                printf(esc_html__('One thought on &ldquo;%1$s&rdquo;', 'pp_www'),
                       '<span>' . get_the_title() . '</span>');
            } else {
                /* WPCS: XSS OK.
                   translators: 1: comment count number, 2: title. */
                printf(esc_html(_nx('%1$s kommentar på &ldquo;%2$s&rdquo;', '%1$s kommentarer på &ldquo;%2$s&rdquo;',
                       $pp_www_comment_count, 'comments title', 'pp_www')),
                       number_format_i18n( $pp_www_comment_count ), '<span>' . get_the_title() . '</span>');
            }
            ?>
        </h2><!-- .comments-title -->
        <?php the_comments_navigation(); ?>
        <ol class="comment-list">
            <?php
            wp_list_comments([
                'style'      => 'ol',
                'short_ping' => true,
            ]);
            ?>
        </ol><!-- .comment-list -->
        <?php
        the_comments_navigation();

        // If comments are closed and there are comments, let's leave a little note, shall we?
        if (! comments_open()) {
            ?>
            <p class="no-comments"><?php esc_html_e('Kommentarsfunktionen är stängd.', 'pp_www'); ?></p>
            <?php
        }
    }

    // Check for have_comments().
    comment_form();
    ?>
</div><!-- #comments -->
