<?php
if (!function_exists('render_three_tabs_with_header')) {
    function render_three_tabs_with_header()
    {
        $columns = get_sub_field('three_tabs');
        $tabContent = [];
        $anchor = get_sub_field('anchor');
        $background = get_sub_field('background');
        $title = get_sub_field('title');
    ?>
        <div style="background-image: url(<?= $background['url'] ?>)" class='buttons_banner_bg big_header'>
            <div class='buttons_banner_container'>
                <section>
                    <article>
                        <h1 class="buttons_banner_h1"><?= $title; ?></h1>
                    </article>
                </section>
            </div>
        </div>
        <?php $rubrik_banner = get_sub_field('rubrik-banner'); ?>
        <div id="<?= $anchor; ?>" class='full-width-banner rubrik_header'>
            <section>
                <article>
                    <h3 class="_text"><?= $rubrik_banner; ?></h3>
                </article>
            </section>
        </div>
        <section>
            <article>
                <div class="row three-tabs tab_buttons">
                    <?php
                    $i = 0;
                    foreach ($columns as $col) {
                        $tabId = substr(md5(microtime()), 0, 7);
                    ?>
                        <div class="tab_three" style="background-color:<?= $col['color_hex']; ?>;">
                            <div data-tabid="<?= $tabId; ?>" class="tab_button">
                                <p><?= apply_filters('the_content', $col['three_tabs_text']); ?></p>
                            </div>
                        </div>
                    <?php
                        $tabContent[] = [
                            'link' => $col['link'],
                            'tabid' => $tabId,
                            'wysiwyg' => $col['wysiwyg'],
                            'color_hex' => $col['color_hex']
                        ];

                        $i++;
                    }
                    ?>
                </div>
                <div class="tabContent">
                    <div class="tabContent_inner tab_content_panel">
                        <?php
                            $i = 0;
                            foreach ($tabContent as $tab) {
                                $tabID = $tab['tabid'];
                                $tab_color_hex = $tab['color_hex'];
                                $tab_content = $tab['wysiwyg'];
                                if (!empty($tab['link'])) {
                                    $tab_read_more = '<p><a class="read-more" href="' . $tab['link']['url'] . '">Läs mer..&raquo;</a></p>';
                                }
                        ?>
                        <div style="background-color: <?= $tab_color_hex ?>" class="_content <?= $tabID ?> adjust">
                            <div class="_content_fixer">
                                <div class="col-12 offset-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                                <?= $tab_content ?>
                                <?= $tab_read_more ?>
                                </div>
                            </div>
                        </div>
                        <?php
                                $i++;
                            }
                        ?>
                    </div>
                </div>
            </article>
        </section>
<?php
    }
}
?>