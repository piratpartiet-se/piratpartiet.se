<?php

if (!function_exists('render_newslist')) {
    function render_newslist (
        $post_count,
        $post_order,
        $no_posts_msg="",
        $filter_by_tag="",
        $banner=FALSE,
        $banner_title="",
        $anchor="",
        $show_archive_link=TRUE
    ) {
        $args = [
            'posts_per_page'    => $post_count,
            'orderby'           => 'date',
            'order'             => $post_order,
            'post_type'         => 'post',
        ];

        if (!empty($filter_by_tag)) {
            $more_args = ['tag' => $filter_by_tag];
            $args = array_merge($args, $more_args);
        }

        $news = get_posts($args);

        if ($banner == TRUE) {
            ?>

            <div id="<?= $anchor ?>" class="full-width-banner pp-orange">
                <h3 class="_text"><?= $banner_title ?></h3>
            </div>


            <?php
        }
    ?>
    <section class="news-list adjust">
    <?php
        foreach ($news as $news_item) {
            $p_date = strftime('%d %B %Y', strtotime($news_item->post_date));

            $p_cat = get_the_category($news_item->ID)[0]->name;
            $p_cat_id = get_cat_ID($p_cat);
            $p_cat_url = get_category_link($p_cat_id);
            $p_cat_link = '[<a href="' . esc_url($p_cat_url) . '" class="cat-link" title="' . $p_cat . '">' . $p_cat . '</a>] ';
            $p_cats = $p_cat_link ?: '';

            $p_excerpt = wpautop($p_cats . $news_item->post_excerpt);
            $p_url = get_permalink($news_item->ID);

            if (has_post_thumbnail()) {
                $p_preview = get_the_post_thumbnail($news_item->ID, 'thumbnail', [
                    'class' => 'alignleft _thumb',
                    'title' => $news_item->post_title,
                ]);
            }

            echo <<< NEWSLIST
        <article>
            <div class="col-xs-12 offset-xs-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                <h3><span class="date">{$p_date} &mdash;</span> {$news_item->post_title}</h3>
                {$p_preview}
                {$p_excerpt}
                <p><a href="{$p_url}"><span class="read-more">Läs mer..&raquo;</span></a></p>
            </div>
        </article>

NEWSLIST;
        }

        if (empty($news)) {
            $no_posts_msg = $no_posts_msg ?: 'För närvarande finns det inga nyheter om %1s.';
            $no_posts_msg = sprintf($no_posts_msg, $filter_by_tag);
            echo <<< NEWSLIST
        <article>
            <div class="col-xs-12 offset-xs-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                <p><em>{$no_posts_msg}</em></p>
            </div>
        </article>

NEWSLIST;
        }

        if ($show_archive_link == true) {
            ?>
        <article>
            <div class="col-xs-12 offset-xs-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                <p>Läs fler nyheter på sidan: <a href="/senaste-nytt/">Senaste nytt..&raquo;</span></a></p>
            </div>
        </article>
            <?php
        }
        ?>
    </section>
    <?php
    }
}
?>
