<?php
if (!function_exists('render_three_tabs')) {
    function render_three_tabs()
    {
        $columns = get_sub_field('three_tabs');
        $tabContent = [];
        $anchor = get_sub_field('anchor');
    ?>
    <section id="<?= $anchor; ?>">
        <div class="row three-tabs tab_buttons">
            <?php
            $i = 0;
            foreach ($columns as $col) {
                $tabId = substr(md5(microtime()), 0, 7);
            ?>
                <div class="tab_three" style="background-color:<?= $col['color_hex']; ?>;">
                    <div data-tabid="<?= $tabId; ?>" class="tab_button">
                        <p><?= apply_filters('the_content', $col['three_tabs_text']); ?></p>
                    </div>
                </div>
            <?php
                $tabContent[] = [
                    'tabid' => $tabId,
                    'wysiwyg' => $col['wysiwyg'],
                    'color_hex' => $col['color_hex'],
                    'extra_html' => $col['extra_html']
                ];

                $i++;
            }
            ?>
        </div>
        <div class="tabContent">
            <div class="tabContent_inner tab_content_panel">
                <?php
                $i = 0;
                foreach ($tabContent as $tab) {
                ?>
                    <article>
                        <div style="background-color:<?= $tab['color_hex']; ?>;" class="_content <?= $tab['tabid']; ?> adjust">
                            <div class="_content_fixer">
                                <div class='col-12 offset-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3'>
                                    <?= apply_filters('the_content', $tab['wysiwyg']); ?>
                                    <?= $tab['extra_html']; ?>
                                </div>
                            </div>
                        </div>
                    </article>
                <?php
                    $i++;
                }
                ?>
            </div>
        </div>
    </section>
<?php
    }
}
?>