<?php
if (!function_exists('render_slider')) {
    function render_slider()
    {
        $slides = get_sub_field('slides');
        $post_count = get_sub_field('post_count');
        $post_order = 'DESC';
        $columns = get_sub_field('three_tabs');
        $tabContent = [];
        $anchor = get_sub_field('anchor');
    ?>
        <div id="slider" class="carousel slide slideshow" data-ride="carousel">
            <ol class="carousel-indicators">
                <?php
                for ($index = 0; $index < count($slides) + $post_count; $index++) {
                ?>
                    <li data-target="#slider" data-slide-to="<?= $index ?>" class="<?= $index === 0 ? 'active' :  '' ?>"></li>
                <?php
                }
                ?>
            </ol>
            <div class="carousel-inner">
                <?php
                $first_slide = true;
                foreach ($slides as $slide) {
                    $link = $slide['link'];
                ?>
                    <div class="carousel-item full-image-slides <?= $first_slide ? 'active' :  '' ?>">
                        <div
                            style="background-image: url(<?= $slide['image']['url'] ?>); <?= $link == "" ? '' : 'cursor: pointer;' ?>"
                            class='buttons_banner_bg big_header'
                            <?= $link == "" ? '' : 'onclick="window.open(\'' . $link . '\');"' ?>
                        >
                            <div class='buttons_banner_container'>
                                <section>
                                    <article>
                                        <h1 class="buttons_banner_h1"><?= $slide['title']; ?></h1>
                                    </article>
                                </section>
                            </div>
                        </div>
                    </div>
                <?php
                    $first_slide = false;
                }
                ?>
                <?php
                $args = [
                    'posts_per_page'    => $post_count,
                    'orderby'           => 'date',
                    'order'             => $post_order,
                    'post_type'         => 'post',
                ];

                $news = get_posts($args);
                foreach ($news as $new) {
                    $p_cat = get_the_category($new->ID)[0]->name;
                    $p_cat_id = get_cat_ID($p_cat);
                    $p_cat_url = get_category_link($p_cat_id);
                    $p_cat_link = '[<a href="' . esc_url($p_cat_url) . '" class="cat-link" title="' . $p_cat . '">' . $p_cat . '</a>] ';
                    $p_cats = $p_cat_link ?: '';

                    $p_excerpt = $new->post_excerpt;
                    $p_url = get_permalink($new->ID);

                    $max_characters = 500;

                    if (strlen($p_excerpt) > $max_characters) {
                        $p_excerpt = trim($p_excerpt);
                        $p_excerpt = substr($p_excerpt, 0, $max_characters);
                        $p_excerpt .= "...";
                    }

                    $p_excerpt = $p_cats . $p_excerpt;

                    if (has_post_thumbnail($new->ID)) {
                        $image = wp_get_attachment_image_src(get_post_thumbnail_id($new->ID), 'full');
                    } else {
                        $image = "";
                    }
                ?>
                    <div class="carousel-item news-slide">
                        <div class="row" style="height: 70%;">
                            <div class="col-sm-12">
                                <a href="<?= $p_url ?>" class="news-slider-image" style="background-image: url('<?= $image[0] ?>');"></a>
                            </div>
                        </div>
                        <div class="row row-news-slider-text" style="height: 30%;">
                            <div class="col-sm-12 news-slider-text">
                                <h1><?= $new->post_title ?></h1>
                                <?= $p_excerpt ?>
                                <p><a href="<?= $p_url ?>"><span class="read-more">Läs mer..&raquo;</span></a></p>
                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>
            <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <?php $rubrik_banner = get_sub_field('rubrik-banner'); ?>
        <div id="<?= $anchor; ?>" class='full-width-banner slider-banner rubrik_header'>
            <section>
                <article>
                    <h3 class="_text"><?= $rubrik_banner; ?></h3>
                </article>
            </section>
        </div>
        <section>
            <article>
                <div class="row three-tabs slider-tabs tab_buttons">
                    <?php
                    $i = 0;
                    foreach ($columns as $col) {
                        $tabID = substr(md5(microtime()), 0, 7);
                    ?>
                        <div class="tab_three" style="background-color:<?= $col['color_hex']; ?>;">
                            <div data-tabid="<?= $tabID; ?>" class="tab_button">
                                <p><?= apply_filters('the_content', $col['three_tabs_text']); ?></p>
                            </div>
                        </div>
                    <?php
                        $tabContent[] = [
                            'link' => $col['link'],
                            'tabid' => $tabID,
                            'wysiwyg' => $col['wysiwyg'],
                            'color_hex' => $col['color_hex']
                        ];

                        $i++;
                    }
                    ?>
                </div>
                <div class="tabContent">
                    <div class="tabContent_inner tab_content_panel">
                        <?php
                        $i = 0;
                        foreach ($tabContent as $tab) {
                            $tabID = $tab['tabid'];
                            $tab_color_hex = $tab['color_hex'];
                            $tab_content = $tab['wysiwyg'];

                            if (!empty($tab['link'])) {
                                $tab_read_more = '<p><a class="read-more" href="' . $tab['link']['url'] . '">Läs mer..&raquo;</a></p>';
                            } else {
                                $tab_read_more = '';
                            }
                        ?>
                        <div style="background-color: <?= $tab_color_hex ?>" class="_content adjust <?= $tabID ?>">
                            <div class="_content_fixer">
                                <div class="col-12 offset-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                                <?= $tab_content ?>
                                <?= $tab_read_more ?>
                                </div>
                            </div>
                        </div>
                        <?php
                            $i++;
                        }
                        ?>
                    </div>
                </div>
            </article>
        </section>
<?php
    }
}
?>