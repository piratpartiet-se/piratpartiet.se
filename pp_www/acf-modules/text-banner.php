<?php
if (!function_exists('render_text_banner')) {
    function render_text_banner()
    {
        $text_banner_text = get_sub_field('text');
        $anchor = get_sub_field('anchor');

        $banner_url = get_sub_field('banner_url');
        $banner_link = sprintf('<a href="%1s">%2s <span class="fas fa-link"></span></a>', $banner_url, $text_banner_text);
        $banner_link_or_title = (!empty($banner_url)) ? $banner_link : $text_banner_text;

?>
        <section id="<?= $anchor ?>" class="text-banner">
            <div class="text-banner-inner full-width-banner">
                <h3 class="_text"><?= $banner_link_or_title ?></h3>
            </div>
        </section>

<?php
    }
}
?>