<?php
if (!function_exists('render_full_width_image')) {
    function render_full_width_image()
    {
        $img = get_sub_field('img');
        $image = $img['sizes']['large'];
        $header_text = $alt = get_sub_field('title') ?: get_the_title();
        $anchor = get_sub_field('anchor');

?>
        <div id="<?= $anchor ?>" title="<?= $alt ?>" class="fullbreddsbild header-text" style="background-image: url({<?= $image ?>};">
            <section>
                <article>
                    <h1 class="header_text_h1"><?= $header_text ?></h1>
                </article>
            </section>
        </div>
<?php
    }
}
?>