<?php

/**
 * Template Name: Page home
 *
 * [!!!] Note: Ok, den här filen är omskriven ganska mycket, ffa formateringen
 * och uppställningen. Eftersom den är så lång, och WP kör “VM i spaghettikod”, så
 * behövdes det få till ngn form av separation - så man får lite koll på all indentering.
 *
 * /E
 *
 * @package pp_www
 */
get_header();

$three_tabs = false;
$slider = false;

?>
<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <?php
        if (have_rows('modules')) {
            while (have_rows('modules')) {
                the_row();
                if (get_row_layout() == 'three_tabs_with_header') {
                    $three_tabs = true;
                    require_once get_template_directory() . '/acf-modules/three-tabs-with-header.php';
                    render_three_tabs_with_header();
                } elseif (get_row_layout() == 'three_tabs_with_slider') {
                    $slider = true;
                    $three_tabs = true;
                    require_once get_template_directory() . '/acf-modules/slider.php';
                    render_slider();
                } elseif (get_row_layout() == 'fullbreddsbild') {
                    require_once get_template_directory() . '/acf-modules/full-width-image.php';
                    render_full_width_image();
                } elseif (get_row_layout() == 'text_banner') {
                    require_once get_template_directory() . '/acf-modules/text-banner.php';
                    render_text_banner();
                } elseif (get_row_layout() == 'text_banner_subheading') {
                    $full_width_textbanner_subheading = get_sub_field('full_width_textbanner_subheading');
                    $link = get_sub_field('link');
                    $link_title = get_sub_field('link_title');
                    $anchor = get_sub_field('anchor');
                ?>
                    <div id="<?= $anchor; ?>" class='full-width-banner'>
                        <section>
                            <article>
                                <a href="<?= $link; ?>" title="<?= $link_title; ?>">
                                    <h3 class="_text"><?= $full_width_textbanner_subheading; ?></h3>
                                </a>
                            </article>
                        </section>
                    </div>
                <?php
                } elseif (get_row_layout() == 'three_tabs') {
                    $three_tabs = true;
                    require_once get_template_directory() . '/acf-modules/three-tabs.php';
                    render_three_tabs();
                } elseif (get_row_layout() == 'text_block') {
                    $id = substr(md5(microtime()), 0, 6);
                    $anchor = get_sub_field('anchor');
                    $text_block_content = apply_filters('the_content', get_sub_field('text_content'));

                    echo <<< TEXTBLOCK
            <div id="{$anchor}" class="text-block adjust">
                <div class="{$id} col-xs-12 offset-xs-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                    {$text_block_content}
                </div>
            </div>

TEXTBLOCK;
                } elseif (get_row_layout() == 'video') {
                    $file = get_sub_field('file');
                    $args = array(
                        'src' => $file['url'],
                        'width' => 1138,
                        'height' => 640,
                        'preload' => 'metadata',
                        'poster' => $file['sizes']['large']
                    );

                    $player = wp_video_shortcode($args);
                    $anchor = get_sub_field('anchor');

                    echo <<< VIDEO
            <section id="{$anchor}">
                <article class="video-wrapper">
                    {$player}
                </article>
            </section>

VIDEO;
                } elseif (get_row_layout() == 'video_youtube') {
                    $video_id = get_sub_field('video_id');
                    $anchor = get_sub_field('anchor');

                    echo <<< YOUTUBEVIDEO
            <section id="{$anchor}">
                <article class="video-wrapper">
                    <iframe src="https://www.youtube-nocookie.com/embed/{$video_id}" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                    </iframe>
                </article>
            </section>

YOUTUBEVIDEO;
                } elseif (get_row_layout() == 'video_vimeo') {
                    $video_id = get_sub_field('video_id');
                    $anchor = get_sub_field('anchor');

                    echo <<< VIMEOVIDEO
            <section id="{$anchor}">
                <article class="video-wrapper">
                    <iframe src="https://player.vimeo.com/video/{$video_id}?loop=1&autopause=1&color=00adef"
                            frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen>
                    </iframe>
                </article>
                <script src="https://player.vimeo.com/api/player.js"></script>
            </section>

VIMEOVIDEO;
                } elseif (get_row_layout() == 'video_twitch') {
                    $video_id = get_sub_field('video_id');
                    $anchor = get_sub_field('anchor');

                    echo <<< TWITCHVIDEO
            <section id="{$anchor}">
                <article class="video-wrapper">
                    <iframe src="https://player.twitch.tv/?autoplay=false&video=v{$video_id}"
                            frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen>
                    </iframe>
                </article>
            </section>

TWITCHVIDEO;
                } elseif (get_row_layout() == 'child_pages') {
                    $children_title = get_sub_field('children_title') ?: get_the_title();
                    $color_class_name = get_sub_field('color_class_name');
                    $children_order = get_sub_field('order_asc_desc');
                    $anchor = get_sub_field('anchor');
                    $no_children_msg = get_sub_field('no_children_msg')
                        ?: 'För närvarande finns det inga undersidor till %1s.'; // ifall-ifall
                    $no_children_msg = sprintf($no_children_msg, $children_title ?: get_the_title());

                    $args = [
                        'order'         => $children_order,
                        'orderby'       => [
                            'menu_order' => $children_order,
                            'post_title' => $children_order
                        ],
                        'post_parent'   => $post->ID,
                        'post_type'     => 'page',
                        'post_status'   => 'publish',
                    ];

                    $children = get_children($args);

                    echo <<< CHILDRENBANNER
            <div id="{$anchor}" class="full-width-banner {$color_class_name}">
                <h3 class="_text">{$children_title}</h3>
            </div>
            <section class="news-list adjust">

CHILDRENBANNER;

                    foreach ($children as $child) {
                        $p_date = strftime('%d %B %Y', strtotime($child->post_date));
                        $p_excerpt = wpautop($child->post_excerpt);
                        $p_url = get_permalink($child->ID);
                        $p_guid = get_permalink($child->ID);

                        if (has_post_thumbnail()) {
                            $p_preview = get_the_post_thumbnail($child->ID, 'thumbnail', [
                                'class' => 'alignleft _thumb',
                                'title' => $child->post_title,
                            ]);
                        }

                        echo <<< CHILD
                <article>
                    <div class="col-xs-12 offset-xs-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                        <h3>{$child->post_title}</h3>
                        {$p_preview}
                        {$p_excerpt}
                        <p><a href="{$p_url}"><span class="read-more">Gå till sidan..&raquo;</span></a></p>
                    </div>
                </article>

CHILD;
                    }

                    if (empty($child)) {
                        echo <<< NOCHILDREN
                <article>
                    <div class="col-xs-12 offset-xs-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                        <p><em>{$no_children_msg}</em></p>
                    </div>
                </article>

NOCHILDREN;
                    }

                    echo str_repeat("\t", 3) . '</section>' . "\n";
                } elseif (get_row_layout() == 'news_list') {
                    $news_title = get_sub_field('news_list_title');
                    $post_count = get_sub_field('post_count');
                    $post_order = get_sub_field('order_asc_desc');
                    $anchor = get_sub_field('anchor');
                    $filter_by_tag = get_sub_field('filter_by_tag') ?: '';
                    $no_posts_msg = get_sub_field('no_posts_msg');

                    require_once get_template_directory() . '/acf-modules/newslist.php';
                    render_newslist($post_count, $post_order, $no_posts_msg, $filter_by_tag, TRUE, $news_title, $anchor);
                }
            }
        }
        ?>
    </main>
</div>
<?php
    if ($three_tabs === true) {
        wp_enqueue_script('pp_www-three-tabs', get_template_directory_uri() . '/js/three-tabs.js', array('jquery'), '1.0', true);
    }
    if ($slider === true) {
        wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '4.4.1', true);
        wp_enqueue_script('pp_www-slider', get_template_directory_uri() . '/js/slider.js', array('jquery', 'bootstrap-js'), '1.0', true);
    }
?>
<?php
get_footer();
?>