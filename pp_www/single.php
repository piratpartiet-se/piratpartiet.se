<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package pp_www
 */
get_header();

$featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
$default_img_url = get_template_directory_uri() . '/img/pp_header_default_clean_3000x1000px.png';
$featured_img_url = $featured_img_url ?: $default_img_url;
$oc_donate_img_url = get_template_directory_uri() . '/img/oc_donate.png';
$paypal_donate_img_url = get_template_directory_uri() . '/img/paypal_donate.webp';
$swish_donate_img_url = get_template_directory_uri() . '/img/swish_qr.png';

$post_title = $post->post_title;
$post_content = apply_filters('the_content', $post->post_content);
$post_date = get_the_date();

echo <<< SINGLE
    <div id="primary" class="content-area">
        <main id="main" class="site-main single">
            <div class="feature" style="background-image: url({$featured_img_url})" title="{$post_title}"></div>
            <div class="full-width-banner">
                <h3 class="_text">{$post_title}</h3>
            </div>
            <div class="content col-xs-12 offset-xs-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                <i>{$post_date}</i>
                <br>
                <br>
                {$post_content}
                <hr>
                <div style="text-align: center;">
                    <h3 class="_text">Donera</h3>
                    <p>
                        Det bästa sättet att ekonomiskt stödja vår verksamhet på är genom att bli vad vi kallar guldpirat - en person som donerar till oss regelbundet, varje månad. Dessa donationer gör att vi kan fortsätta med vår verksamhet och planera för framtiden. Bli guldpirat genom att klicka på knappen nedan.
                    </p>
                    <p>
                        Du kan också donera enskilda belopp via Swish (1231792159) eller PayPal.
                    </p>
                    <b>Guldpirat</b>
                    <br>
                    <a href="https://opencollective.com/piratpartiet/contribute/guldpirat-23683/checkout" target="_blank" rel="noopener">
                        <img src="{$oc_donate_img_url}" width="300">
                    </a>
                    <br>
                    <br>
                    <b>Swish</b>
                    <br>
                    <a href="{$swish_donate_img_url}">
                        <img width="300" src="{$swish_donate_img_url}">
                    </a>
                    <br>
                    <b>PayPal</b>
                    <br>
                    <form action="https://www.paypal.com/donate" method="post" target="_top" style="text-align: center;">
                        <input type="hidden" name="business" value="donation@piratpartiet.se" />
                        <input type="hidden" name="currency_code" value="SEK" />
                        <input type="image" src="{$paypal_donate_img_url}" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donera med PayPal-knappen" />
                    </form>
                    <h3 class="_text">Bli pirat</h3>
                    <p>
                        Att bli medlem i Piratpartiet är det enklaste sättet att visa stöd för vad vi gör. Det är helt gratis, eftersom vi inte tycker att ekonomi ska vara en barriär för att engagera sig i politiken. Välkommen ombord!
                    </p>
                    <a href="https://blipirat.nu">
                        <button type="button" style="width: 40%; max-width: 300px;" class="btn btn-primary">
                            Bli pirat!
                        </button>
                    </a>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

SINGLE;

get_footer();
