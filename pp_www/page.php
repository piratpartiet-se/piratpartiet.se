<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package pp_www
 */
get_header();

$featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
$default_img_url = get_template_directory_uri() . '/img/pp_header_default_clean_3000x1000px.png';
$featured_img_url = $featured_img_url ?: $default_img_url;

$post_title = $post->post_title;
$post_content = apply_filters('the_content', $post->post_content);

echo <<< PAGE
    <div id="primary" class="content-area">
        <main id="main" class="site-main single-news">
            <div class="feature" style="background-image: url({$featured_img_url})" title="{$post_title}"></div>
            <div class="full-width-banner">
                <h3 class="_text">{$post_title}</h3>
            </div>
            <div class="_content col-xs-12 offset-xs-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                {$post_content}
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

PAGE;

get_footer();
