<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pp_www
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <?php
        if (is_singular()) {
            the_title('<h1 class="entry-title">', '</h1>');
        } else {
            the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
        }

        if ('post' === get_post_type()) {
            ?>
            <div class="entry-meta">
                <?php
                pp_www_posted_on();
                pp_www_posted_by();
                ?>
            </div><!-- .entry-meta -->
        <?php } ?>
    </header><!-- .entry-header -->

    <?php pp_www_post_thumbnail(); ?>

    <div class="entry-content">
        <?php
          the_content(sprintf(
            wp_kses(
                // translators: %s: Name of current post. Only visible to screen readers
                __('Fortsätt läs<span class="screen-reader-text"> "%s"</span>', 'pp_www'),
                [
                    'span' => [
                        'class' => [],
                    ],
                ]
            ),
            get_the_title()
        ));

        wp_link_pages([
            'before' => '<div class="page-links">' . esc_html__('Pages:', 'pp_www'),
            'after'  => '</div>',
        ]);
        ?>
    </div><!-- .entry-content -->
    <footer class="entry-footer">
        <?php pp_www_entry_footer(); ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
