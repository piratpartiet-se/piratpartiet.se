<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pp_www
 */

 /**
  * Den här sidan är för att visa nyheterna inom en kategori,
  * t ex pressmeddelande, allmänt, osv.
  */

$post_url = esc_url(get_permalink());
$post_id = get_the_ID();
//$post_class = get_post_class();
$post_class = join(' ', get_post_class());

$posted_on = pp_www_posted_on() ?? '';
$posted_by = pp_www_posted_by() ?? '';

if (is_singular() || $wp_query->found_posts === '1') {
    $post_title = the_title('<h1 class="entry-title">', '</h1>', false);
    $post_content = get_the_content(sprintf(
            wp_kses(
                // translators: %s: Name of current post. Only visible to screen readers
                __('Fortsätt läs<span class="screen-reader-text"> "%s"</span>', 'pp_www'),
                [
                    'span' => [
                        'class' => [],
                    ],
                ]
            ),
            get_the_title()
        ));
} else {
    $post_title = the_title('<h2 class="entry-title"><a href="' . $post_url . '" rel="bookmark">', '</a></h2>', false);
    $post_content = get_the_excerpt() . ' &hellip;';
    $post_read_more = '<p><a href="' . $post_url .'"><span class="read-more">Läs mer..&raquo;</span></a></p>';

}

echo <<< CATBANNER
                <article id="post-{$post_id}" {$post_class}>
                    <div class="col-xs-12 offset-xs-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                        {$post_title}

CATBANNER;

if ('post' === get_post_type()) {
    echo <<< META
                        <div class="entry-meta">
                            <p>{$posted_on}, {$posted_by}.</p>
                        </div><!-- .entry-meta -->

META;
}

// Bilden
pp_www_post_thumbnail();

echo <<< CATPOST
                        <p>{$post_content}</p>
                        {$post_read_more}
                    </div>
                </article>

CATPOST;
