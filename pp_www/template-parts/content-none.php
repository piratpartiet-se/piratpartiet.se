<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pp_www
 */

$nothing_found = esc_html__('Inget kunde hittas', 'pp_www');
$post_class = join(' ', get_post_class());
$search_form = get_search_form(false);

echo <<< CONTENT
            <section class="no-results not-found">
                <article id="post-{$post_id}" {$post_class}>
                    <div class="col-xs-12 offset-xs-0 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
                        <header class="page-header">
                            <h1 class="page-title">{$nothing_found}</h1>
                        </header><!-- .page-header -->
                        <div class="page-content">

CONTENT;

if (is_home() && current_user_can('publish_posts')) {
    printf(
            '<p>' . wp_kses(
            /* translators: 1: link to WP admin new post page. */
            __('Redo att publicera ditt första inlägg? <a href=\"%1$s\">Starta här</a>.', 'pp_www'),
            [
                'a' => [
                    'href' => [],
                ],
            ]
        ) . '</p>',
        esc_url(admin_url('post-new.php'))
    );
} elseif (is_search()) {
    $no_results = esc_html__('Tyvärr, men ingenting matchade din sökning. Prova igen med några andra sökord.', 'pp_www');

    echo <<< NORESULTS
                            <p>{$no_results}</p>
                            {$search_form}

NORESULTS;
} else {
    $no_results = esc_html__('Tyvärr kan vi inte hitta det du letade efter. Kanske en sökning kan hjälpa.', 'pp_www');

    echo <<< NORESULTS
                            <p>{$no_results}</p>
                            {$search_form}

NORESULTS;
}
?>
                        </div><!-- .page-content -->
                    </div>
                </article><!-- $post_id -->
            </section><!-- .no-results -->
