# Hjälpa till

Om du vill hjälpa till med att bygga upp sidan så är det jättebra. Här följer några tips och råd, och de rutiner du bör följa.

1. Kolla om det finns ett [ärende][issues] öppnat, så du kan referera till det i meddelandet.
2. Om du vill/behöver, så skapa ett [nytt ärende][issues] för att diskutera eller för att få hjälp. Du kan även komma med förslag.
3. Ifall du skriver kod, följ kodstandard.
4. Följ vårt arbetssätt som vi använder oss av här på GitLab.
5. Gör inga ändringar i `master`, den branchen motsvarar vad som finns uppladdat på [piratpartiet.se][pp]

## Lokal miljö

När du utvecklar så vill du nog testa dina förändringar. Det lättaste sättet är att sätta upp en lokal WordPress instans och köra där. Du måste dock kunna köra ACF Pro på din lokala instans.

## Riktlinjer

Se till att du följer de riktilinjer, och den kodstandard vi satt upp - så alla gör likadant.

### PHP
 - Kodstandard: [PSR-12][https://www.php-fig.org/psr/psr-12/]


### Fork

Vi följer [Gitflow][gitflow].

1. Gör en fork av repot <https://gitlab.com/piratpartiet-se/piratpartiet.se/>
2. Skapa en egen branch för din fix/ändring `git checkout -b feature/fooBar`. OBS! Se till att att din branch är baserad på
3. Lägg till/spara ändringarna `git commit -am 'Add some fooBar'`. Alla commits ska vara på engelska
4. Pusha koden till din nya branch `git push origin feature/fooBar`
5. Skapa en ny “Merge Request” till branchen `develop`

Endast kärnutvecklare får skapa branches direkt i repot: <https://gitlab.com/piratpartiet-se/piratpartiet.se/>. Dock så ska varje branch tas bort när den blir mergad till `develop`.

<!-- Länkar och bilder -->
[pp]: https://piratpartiet.se/
[issues]: https://gitlab.com/piratpartiet-se/piratpartiet.se/issues
[gitflow]: https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow